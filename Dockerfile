# PHPUnit Docker Container.
ARG PHP_VERSION

FROM php:${PHP_VERSION}-alpine

LABEL maintainer="kaspar.wetsch@gmail.com"

# Set PHPUNIT version variables
ARG PHPUNIT_VERSION

# Run some Debian packages installation.
ENV PACKAGES="php-pear curl"

# Goto temporary directory.
WORKDIR /tmp

# Install PHP packages
RUN apk add --no-cache git

# Get composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Run composer and phpunit installation.
RUN composer require phpunit/phpunit ^${PHPUNIT_VERSION} --prefer-source --no-interaction && \
    ln -s /tmp/vendor/bin/phpunit /usr/local/bin/phpunit

# Set up the application directory.
VOLUME ["/app"]
WORKDIR /app

# Set up the command arguments.
ENTRYPOINT ["/usr/local/bin/phpunit"]
CMD ["--help"]
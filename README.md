# PHPUnit on Docker by Kaspar Wetsch


[![pipeline status](https://gitlab.com/kaspar.wetsch/phpunitondocker/badges/master/pipeline.svg?style=flat-square
)](https://gitlab.com/kaspar.wetsch/phpunitondocker/commits/master)

## Installation / Usage

1. Pull the phpunit container:

    ``` sh
	$ docker pull registry.gitlab.com/kaspar.wetsch/phpunitondocker:latest
	```

2. Create a phpunit.xml defining your tests suites.

    ``` xml
    ...
    ```

3. Run PHPUnit through the PHPUnit container:

    ``` sh
	$ docker run -v $(pwd):/app --rm phpunit run
    ```
  
    you can also check the version via
    ``` sh
	$ docker run -v $(pwd):/app --rm phpunit --check-version
    ```

### Generate a new version

just trigger a new pipeline with following parameters

| Variable        | Version |
|-----------------|---------|
| PHP_VERSION     | eg. 7.2 |
| PHPUNIT_VERSION | eg. 8.4 |
